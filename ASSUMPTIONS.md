### Junior Back End Engineer Test
 
#### Assumptions

- if there's a parameters in `list` or `listUsers` method (filter), it will only return one IUser object.
- default pagination value: `page: 1`, `limit: 10`.