import { IUserManager } from "interfaces/managers";
import { Component } from "merapi";
import { IUserRepo, Paginated } from "interfaces/repos";
import { IUser } from "interfaces/descriptors";
import * as _ from "lodash";
import UserRepo from "components/repos/user_repo";

export default class UserManager extends Component implements IUserManager {
    constructor(private userRepo: IUserRepo) {
        super();
    }

    public listUser(
        query: Object,
        options?: {
            page?: number;
            limit?: number;
            sort?: { [column: string]: "asc" | "desc" };
        }
    ): Paginated<IUser> {
        let result = {} as any;
        result['page'] = 1;
        result['limit'] = 10;
        const users = this.userRepo.list({});
        if (_.isEmpty(query)) {
            result['data'] = users;
        }
        else {
            const qry: any = query;
            const filteredUser = this.readUser(qry.username);
            result['data'] = [filteredUser];
        }
        if (!_.isEmpty(options)) {
            for (const param in options.sort) {
                result.data = result.data.sort(function(obj1:any, obj2:any) {
                    if(options.sort[param] === 'asc') {
                        return obj1[param] - obj2[param] ;
                    }
                    else {
                        return obj2[param] - obj1[param] ;
                    }
                });
            }
            if (options.page){
                result['page'] = options.page;
            }
            if (options.limit) {
                result['limit'] = options.limit;
            }
        }
        result['total'] = this.userRepo.count({});
        return result;
    }

    public descendingUsername(): Paginated<IUser> {
        return this.listUser({}, { sort: { username: "desc" } });
    }

    public readUser(username: string): IUser {
        const user = this.userRepo.read(username);
        if (!user) {
            throw new Error(`User with username ${username} not found.`);
        }
        return user;
    }

    public deleteUser(username: string): boolean {
        const user = this.userRepo.delete(username);

        if (!user) {
            throw new Error(`User with username ${username} not found.`);
        }

        else {
            return true;
        }


        
    }

    public register(object: IUser): IUser {
        const { username, email, password } = object;

        if (!username || !email || !password) {
            throw new Error("Username, email, and password are required.");
        }

        const user = this.userRepo.create(object);
        if (!user) {
            throw new Error(`User with username ${username} already exists.`);
        }
        return user;
    }

    public login(username: string, password: string): boolean {
        const user = this.userRepo.read(username);
        if (!user) {
            throw new Error(`User with username ${username} not found.`);
        }
        else {
            if (user.password === password) {
                return true;
            }

            else {
                throw new Error(`Incorrect password for user with username ${username}.`);      
            }
        }
    }

    public updateUser(username: string, object: IUser): IUser {
        const updated = this.userRepo.update(username, object);
        if (object.password) {
            throw new Error("Can't update user's password using updateUser method.");                  
        }
        else {
            if (updated) {
                return updated;
            }
            else {
                throw new Error(`User with username ${username} not found.`);
                
            }
        }
    }

    public updatePassword(
        username: string,
        oldPassword: string,
        newPassword: string
    ): IUser {
        const user = this.userRepo.read(username);
        if (!user){
            throw new Error(`User with username ${username} not found.`);
        }
        else {
            if (user.password === oldPassword) {
                const updated = this.userRepo.update(username, {password: newPassword});
                return updated;
            }
            else {
                throw new Error("Old password doesn't match");
            }
        }
    }
}
