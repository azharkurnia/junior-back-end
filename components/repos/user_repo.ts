import { IUserRepo } from "interfaces/repos";
import { IUser } from "interfaces/descriptors";
import * as _ from "lodash";
import { Component } from "merapi";

export default class UserRepo extends Component implements IUserRepo {
    constructor() {
        super();
    }

    private users: { [username: string]: IUser } = {};

    public list(
        query: Object,
        options?: {
            page?: number;
            limit?: number;
            sort?: { [column: string]: "asc" | "desc" };
        }
    ): IUser[] {
        let res = [];
        if (_.isEmpty(query)) {
            for (const x in this.users) {
                res.push(this.users[x]);  
            }
        }
        else {
            const qry: any = query;
            res.push(this.users[qry.username]);
        }
        
        if (!_.isEmpty(options)) {
            for (const param in options.sort) {
                res = res.sort(function(obj1, obj2) {
                    var objj1: any = obj1;
                    var objj2: any = obj2
                    if(options.sort[param] === 'asc') {
                        return objj1[param] - objj2[param] ;
                    }
                    else {
                        return objj2[param] - objj1[param] ;
                    }
                });
            }
            if (options.page || options.limit) {
                return _.chunk(res,options.limit)[options.page-1];
            }
        }
        
        return res;
    }

    public count(query: Object): number {
        if (_.isEmpty(query)) {
            return _.size(this.users);
        }

        else {
            let tmp = [];
            const qry: any = query;
            let selected = this.read(qry);
            if (selected === null) {
                return 0
            }
            tmp.push(selected);
            return _.size(tmp);
        }
    }

    public create(object: IUser): IUser {
        const { username } = object;

        if (this.users[username]) {
            return null;
        }

        this.users[username] = object;
        return object;
    }

    public read(username: string): IUser {
        if (this.users[username]) {
            return this.users[username];
        }
        else {
            return null;
        }
    }

    public update(username: string, object: Partial<IUser>): IUser {
        let selected = this.read(username);

        if (selected) {
            selected.email = object.email;
            selected.name = object.name;
            selected.password = object.password;
            selected.username = object.username;
            return selected;
        }
        else {
            return null;
        }
    }

    public delete(username: string): boolean {
        let selected = this.read(username);

        if (selected) {
            delete this.users[username];
            return true;
        }
        else {
            return false;
        }
    }

    public clear(): void {
        this.users = {};
    }
}
